json.extract! first_model, :id, :foo, :bar, :created_at, :updated_at
json.url first_model_url(first_model, format: :json)

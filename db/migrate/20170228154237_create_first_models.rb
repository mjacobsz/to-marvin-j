class CreateFirstModels < ActiveRecord::Migration
  def change
    create_table :first_models do |t|
      t.string :foo
      t.string :bar

      t.timestamps null: false
    end
  end
end
